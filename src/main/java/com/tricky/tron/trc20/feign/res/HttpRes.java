package com.tricky.tron.trc20.feign.res;

import lombok.Data;

@Data
public class HttpRes {
    private Result result;
    private Transaction transaction;
}
