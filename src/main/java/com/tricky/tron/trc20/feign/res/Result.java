package com.tricky.tron.trc20.feign.res;

import lombok.Data;

@Data
public class Result {
    private Boolean result;
}
