package com.tricky.tron.trc20.feign.res;

import lombok.Data;

@Data
public class Transaction {
    private String signature;
    private String txID;
}
