package com.tricky.tron.trc20.service;

import com.alibaba.fastjson.JSONObject;
import com.tricky.tron.trc20.controller.WalletController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

@Service
public class TRXService {
    @Autowired
    private Trc20Service trc20Service;
    @Autowired
    private AccountService accountService;

    private Logger logger = LoggerFactory.getLogger(TRXService.class);

    public String createNewWallet(String account){
        logger.info("====>  Generate new wallet file for TRX.");
        JSONObject  json = trc20Service.createAddress();
        String address = json.getString("address");
        String privateKey = json.getString("privateKey");
        String hexAddress = json.getString("hexAddress");
        accountService.saveOne(account, privateKey, address);
        return address;
    }
}
