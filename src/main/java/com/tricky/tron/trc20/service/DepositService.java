package com.tricky.tron.trc20.service;

import com.tricky.tron.trc20.common.Deposit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class DepositService {
    @Autowired
    private MongoTemplate mongoTemplate;

    public void save(Deposit tx){
        mongoTemplate.insert(tx,getCollectionName());
    }

    public String getCollectionName(){
        return  "TRX_deposit";
    }

    public boolean exists(Deposit deposit){
        Criteria criteria =  Criteria.where("address").is(deposit.getAddress())
                .andOperator(Criteria.where("txid").is(deposit.getTxid()));
        Query query = new Query(criteria);
        return mongoTemplate.exists(query,getCollectionName());
    }


}
