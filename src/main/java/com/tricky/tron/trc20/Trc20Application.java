package com.tricky.tron.trc20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.tricky.tron.trc20.feign"})
public class Trc20Application {

	public static void main(String[] args) {
		SpringApplication.run(Trc20Application.class, args);
	}

}
