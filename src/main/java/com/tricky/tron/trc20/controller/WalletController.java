package com.tricky.tron.trc20.controller;

import com.alibaba.fastjson.JSONObject;
import com.tricky.tron.trc20.common.MessageResult;
import com.tricky.tron.trc20.entity.Account;
import com.tricky.tron.trc20.service.AccountService;
import com.tricky.tron.trc20.service.TRXService;
import com.tricky.tron.trc20.service.Trc20Service;
import org.apache.tomcat.util.compat.JreCompat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/test")
public class WalletController {

    @Autowired
    private Trc20Service trc20Service;
    @Autowired
    private AccountService accountService;

    @Autowired
    private TRXService service;

    private Logger logger = LoggerFactory.getLogger(WalletController.class);

    /**
     * 发送trc20交易
     *
     * @param address
     * @param amount
     * @param remark
     * @return
     */
    @RequestMapping("/send")
    public String sendTx(@RequestParam("address") String address, @RequestParam("amount") String amount, @RequestParam("remark") String remark) {
        String tx = trc20Service.sendTrc20Transaction(address, amount, remark);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("title", "发送Trc20交易");
        if (tx != null) {

            resultMap.put("result", "success");
            resultMap.put("txid", tx);
        } else {
            resultMap.put("result", "fail");
        }
        return JSONObject.toJSONString(resultMap);
    }

    /**
     * 查询额度
     *
     * @param address
     * @param contract
     * @return
     */
    @RequestMapping("/queryToken")
    public String queryToken(@RequestParam("address") String address, @RequestParam("contract") String contract) {
        Map<String, Object> map = new HashMap<>();
        map.put("title", "查询代币");
        map.put("address", address);
        map.put("contract", contract);
        BigInteger amount = trc20Service.balanceOf(contract, address);
        map.put("amount", amount);
        return JSONObject.toJSONString(map);
    }


    /**
     * 发起转账
     *
     * @param address
     * @param contract
     * @return
     */
    @RequestMapping("/sendToken")
    public String sendToken(@RequestParam("address") String address, @RequestParam("contract") String contract,
                            @RequestParam("amount") String amount, @RequestParam("remark") String remark,
                            @RequestParam("get") String get, @RequestParam("pk") String pk) {
        Map<String, Object> map = new HashMap<>();
        map.put("标题", "查询代币");
        map.put("付款地址", address);
        map.put("合约", contract);
        map.put("私钥", pk);
        map.put("收款地址", get);
        map.put("额度", amount);
        map.put("备注", remark);
        String tx = trc20Service.sendTokenTransaction(contract, address, pk, amount, get, remark);
        if (tx != null) {
            map.put("结果", "成功");
            map.put("交易id", tx);
        } else {
            map.put("结果", "失败");
        }
        return JSONObject.toJSONString(map);
    }


    /**
     * 获取波场币 trx的数量
     *
     * @param address
     * @return
     */
    @RequestMapping("/balanceOfTron")
    public String balanceOfTron(@RequestParam("address") String address) {
        Map<String, Object> map = new HashMap<>();
        map.put("标题", "获取波场币数量");
        map.put("地址", address);
        BigDecimal decimal = trc20Service.balanceOfTron(address);
        map.put("数量", decimal.toString());
        return JSONObject.toJSONString(map);
    }
    @RequestMapping("/easytransferbyprivate")
    public String easytransferbyprivate(@RequestParam("toAddress") String toAddress,@RequestParam("privateKey") String privateKey,@RequestParam("amount") String amount) {
        Map<String, Object> map = new HashMap<>();
        map.put("标题", "直接转账trx");
        map.put("目标地址", toAddress);
        String result = trc20Service.easytransferbyprivate(toAddress,privateKey,amount);
        map.put("数量", amount);
        return result;
    }

    /*获取一个新的trc20地址*/
    @GetMapping("address/{account}")
    public MessageResult getNewAddress(@PathVariable String account) {
        logger.info("create new account={}", account);
        try {
            String address;
            Account acct = accountService.findByName("TRX",account);
            if(acct != null){
                address = acct.getAddress();
                accountService.save(acct,"TRX");
            }
            else {
                address = service.createNewWallet(account);
            }
            MessageResult result = new MessageResult(0, "success");
            result.setData(address);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return MessageResult.error(500, "rpc error:" + e.getMessage());
        }
    }


    /**
     * 创建tron地址
     *
     * @return
     */
    @RequestMapping("/createAddress")
    public String createAddress() {
        JSONObject obj = trc20Service.createAddress();
        obj.put("title", "创建地址");
        return obj.toJSONString();
    }
}
